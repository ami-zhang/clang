%bcond_without sys_llvm
%bcond_without check
%bcond_with classic_flang
%bcond_with toolchain_clang
%bcond_without bisheng_autotuner

%if %{with toolchain_clang}
%global toolchain clang
%endif

%global maj_ver 17
%global min_ver 0
%global patch_ver 6
%global clang_version %{maj_ver}.%{min_ver}.%{patch_ver}

%if %{with sys_llvm}
%global pkg_name clang
%global install_prefix %{_prefix}
%global install_datadir %{_datadir}
%else
%global pkg_name clang%{maj_ver}
%global install_prefix %{_libdir}/llvm%{maj_ver}
%global install_datadir %{install_prefix}/share
%endif

%global install_bindir %{install_prefix}/bin
%global install_includedir %{install_prefix}/include
%if 0%{?__isa_bits} == 64
%global install_libdir %{install_prefix}/lib64
%else
%global install_libdir %{install_prefix}/lib
%endif
%global install_libexecdir %{install_prefix}/libexec
%global install_sharedir %{install_prefix}/share
%global install_docdir %{install_sharedir}/doc

%global clang_srcdir clang-%{clang_version}.src
%global clang_tools_srcdir clang-tools-extra-%{clang_version}.src
%global max_link_jobs %{_smp_build_ncpus}

# Disable LTO as this causes crash if gcc lto enabled.
%define _lto_cflags %{nil}

Name:		%{pkg_name}
Version:	%{clang_version}
Release:	22
Summary:	A C language family front-end for LLVM

License:	NCSA
URL:		http://llvm.org
Source0:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{clang_version}/%{clang_srcdir}.tar.xz
Source1:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{clang_version}/%{clang_tools_srcdir}.tar.xz

Patch0: 	fedora-PATCH-clang-Don-t-install-static-libraries.patch
Patch1:		0001-Add-triples-for-X86_64-AArch64-Riscv64-openEuler-gcc.patch
Patch2:		0002-Revert-Clang-Change-the-default-DWARF-version-to-5.patch
Patch3:		0003-add-BUILD_FOR_OPENEULER-build-option-to-clang.patch
Patch4:		0004-add-gcc-compatible-in-BUILD_FOR_OPENEULER.patch
Patch5:		0005-backport-Disable-InterpreterExceptionTest-on-RISC-V.patch
Patch6:		0006-clang-LoongArch-Add-loongarch64-to-os-triple.patch
Patch7:		0007-add-more-warning-options-to-fgcc-compatible.patch
Patch8:		0008-Backport-LoongArch-Add-the-support-for-vector.patch
Patch9:		0009-Backport-LoongArch-improve-the-support-for-compiler-rt-and-bugfix.patch
Patch10:        0010-Backport-clang-Avoid-Wshadow-warning-when-init-capture-named.patch
Patch11:        0011-Add-the-support-for-classic-flang.patch
Patch12:        0012-Fix-declaration-definition-mismatch-for-classic-flang.patch
Patch13:        0013-Ignored-option-Wa-generate-missing-build-notes.patch
Patch14:        0014-Update-llvm-lit-config-to-support-build_for_openeule.patch
Patch15:        0015-Backport-Defer-the-instantiation-of-explicit-specifier-until-.patch
Patch16:        0016-Add-BiSheng-Autotuner-support-for-LLVM-compiler.patch
Patch17:        0017-fix-for-missing-DENABLE_AUTOTUNER.patch
Patch18:        0018-backport-Clang-Fix-build-with-GCC-14-on-ARM-78704.patch

# Patches for clang-tools-extra
# See https://reviews.llvm.org/D120301
Patch201:	fedora-clang-tools-extra-Make-test-dependency-on-LLVMHello-.patch

BuildRequires:	gcc
BuildRequires:	gcc-c++
BuildRequires:	cmake
BuildRequires:	libatomic

%if %{with sys_llvm}
BuildRequires:	llvm-libs = %{version}
BuildRequires:	llvm-devel = %{version}
BuildRequires:	llvm-static = %{version}
BuildRequires:	llvm-test = %{version}
BuildRequires:	llvm-googletest = %{version}
BuildRequires:	llvm-cmake-utils = %{version}
%else
BuildRequires:	llvm%{maj_ver}-libs = %{version}
BuildRequires:	llvm%{maj_ver}-devel = %{version}
BuildRequires:	llvm%{maj_ver}-static = %{version}
BuildRequires:	llvm%{maj_ver}-test = %{version}
BuildRequires:	llvm%{maj_ver}-googletest = %{version}
BuildRequires:	llvm%{maj_ver}-cmake-utils = %{version}
%endif

BuildRequires:	libxml2-devel
BuildRequires:	multilib-rpm-config
BuildRequires:	ninja-build
BuildRequires:	ncurses-devel
BuildRequires:	perl-generators
BuildRequires:	python3-lit >= %{version}
BuildRequires:	python3-sphinx
BuildRequires:	python3-recommonmark
BuildRequires:	python3-devel

BuildRequires: perl(Digest::MD5)
BuildRequires: perl(File::Copy)
BuildRequires: perl(File::Find)
BuildRequires: perl(File::Path)
BuildRequires: perl(File::Temp)
BuildRequires: perl(FindBin)
BuildRequires: perl(Hash::Util)
BuildRequires: perl(lib)
BuildRequires: perl(Term::ANSIColor)
BuildRequires: perl(Text::ParseWords)
BuildRequires: perl(Sys::Hostname)
%if %{with toolchain_clang}
BuildRequires:	clang
%endif

Requires:	%{name}-libs%{?_isa} = %{version}-%{release}

Requires:	libstdc++-devel
# Require gcc libs installed during rumtime
Requires:	gcc
Requires:	gcc-c++

Provides:	clang(major) = %{maj_ver}

Conflicts:	compiler-rt < 11.0.0

%description
clang: noun
    1. A loud, resonant, metallic sound.
    2. The strident call of a crane or goose.
    3. C-language family front-end toolkit.

The goal of the Clang project is to create a new C, C++, Objective C
and Objective C++ front-end for the LLVM compiler. Its tools are built
as libraries and designed to be loosely-coupled and extensible.

Install compiler-rt if you want the Blocks C language extension or to
enable sanitization and profiling options when building, and
libomp-devel to enable -fopenmp.

%package libs
Summary: Runtime library for clang
Requires: %{name}-resource-filesystem%{?_isa} = %{version}
Recommends: compiler-rt%{?_isa} = %{version}
Recommends: libatomic%{?_isa}
Recommends: libomp-devel%{_isa} = %{version}
Recommends: libomp%{_isa} = %{version}

%description libs
Runtime library for clang.

%package devel
Summary: Development header files for clang
Requires: %{name}-libs = %{version}-%{release}

%description devel
Development header files for clang.

%package resource-filesystem
Summary: Filesystem package that owns the clang resource directory
Provides: %{name}-resource-filesystem(major) = %{maj_ver}

%description resource-filesystem
This package owns the clang resouce directory: $libdir/clang/$version/


%package analyzer
Summary:	A source code analysis framework
License:	NCSA and MIT
BuildArch:	noarch
Requires:	%{name} = %{version}-%{release}

%description analyzer
The Clang Static Analyzer consists of both a source code analysis
framework and a standalone tool that finds bugs in C and Objective-C
programs. The standalone tool is invoked from the command-line, and is
intended to run in tandem with a build of a project or code base.

%package tools-extra
Summary:	Extra tools for clang
Requires:	%{name}-libs%{?_isa} = %{version}-%{release}
Requires:	emacs-filesystem

%description tools-extra
A set of extra tools built using Clang's tooling API.

%package tools-extra-devel
Summary: Development header files for clang tools
Requires: %{name}-tools-extra = %{version}-%{release}
 
%description tools-extra-devel
Development header files for clang tools.

%package -n git-clang-format
Summary:	Integration of clang-format for git
Requires:	%{name}-tools-extra = %{version}-%{release}
Requires:	git
Requires:	python3

%description -n git-clang-format
clang-format integration for git.

%prep
%setup -T -q -b 1 -n %{clang_tools_srcdir}
%autopatch -m200 -p2

# failing test case
#rm test/clang-tidy/checkers/altera/struct-pack-align.cpp

pathfix.py -i %{__python3} -pn \
	clang-tidy/tool/ \
	clang-include-fixer/find-all-symbols/tool/run-find-all-symbols.py

%setup -q -n %{clang_srcdir}
%autopatch -M200 -p2

pathfix.py -i %{__python3} -pn \
	tools/clang-format/ \
	tools/clang-format/git-clang-format \
	utils/hmaptool/hmaptool \
	tools/scan-view/bin/scan-view \
	tools/scan-view/share/Reporter.py \
	tools/scan-view/share/startfile.py \
	tools/scan-build-py/bin/* \
	tools/scan-build-py/libexec/*

%build
mkdir -p _build
cd _build
%cmake .. -G Ninja \
	-DCLANG_DEFAULT_PIE_ON_LINUX=ON \
	-DLLVM_PARALLEL_LINK_JOBS=%{max_link_jobs} \
	-DLLVM_LINK_LLVM_DYLIB:BOOL=ON \
	-DCMAKE_BUILD_TYPE=Release \
	-DPYTHON_EXECUTABLE=%{__python3} \
	-DCMAKE_SKIP_RPATH:BOOL=ON \
	-DCLANG_BUILD_TOOLS:BOOL=ON \
	-DCMAKE_INSTALL_PREFIX=%{install_prefix} \
	-DCLANG_INCLUDE_TESTS:BOOL=ON \
	-DLLVM_EXTERNAL_LIT=%{_bindir}/lit \
	-DLLVM_CONFIG:FILEPATH=%{install_bindir}/llvm-config \
	-DLLVM_TABLEGEN_EXE:FILEPATH=%{install_bindir}/llvm-tblgen \
	-DLLVM_MAIN_SRC_DIR=%{install_prefix}/src \
	-DLLVM_LIT_ARGS="-vv" \
	-DLLVM_BUILD_UTILS:BOOL=ON \
	-DLLVM_COMMON_CMAKE_UTILS=%{install_datadir}/llvm/cmake \
	-DCLANG_ENABLE_ARCMT:BOOL=ON \
	-DCLANG_ENABLE_STATIC_ANALYZER:BOOL=ON \
	-DCLANG_INCLUDE_DOCS:BOOL=ON \
	-DCLANG_PLUGIN_SUPPORT:BOOL=ON \
	-DENABLE_LINKER_BUILD_ID:BOOL=ON \
	-DLLVM_ENABLE_EH=ON \
	-DLLVM_ENABLE_RTTI=ON \
	-DLLVM_BUILD_DOCS=ON \
	-DLLVM_ENABLE_SPHINX=ON \
	-DCLANG_LINK_CLANG_DYLIB=ON \
	-DSPHINX_WARNINGS_AS_ERRORS=OFF \
	-DCLANG_BUILD_EXAMPLES:BOOL=OFF \
	-DBUILD_SHARED_LIBS=OFF \
	-DCLANG_REPOSITORY_STRING="%{?distro} %{version}-%{release}" \
	-DLLVM_EXTERNAL_CLANG_TOOLS_EXTRA_SOURCE_DIR=../../%{clang_tools_srcdir} \
%if 0%{?__isa_bits} == 64
	-DCLANG_RESOURCE_DIR=../lib64/clang/%{maj_ver} \
%else
	-DCLANG_RESOURCE_DIR=../lib/clang/%{maj_ver} \
%endif
%if 0%{?__isa_bits} == 64
	-DLLVM_LIBDIR_SUFFIX=64 \
%else
	-DLLVM_LIBDIR_SUFFIX= \
%endif
%if %{with classic_flang}
        -DLLVM_ENABLE_CLASSIC_FLANG=ON \
%endif
%if %{with bisheng_autotuner}
	-DLLVM_ENABLE_AUTOTUNER=ON \
%endif
	-DBUILD_FOR_OPENEULER=ON \
%if "%{toolchain}" == "clang"
	-DCMAKE_C_COMPILER=clang \
	-DCMAKE_CXX_COMPILER=clang++ \
%endif
	-DCLANG_DEFAULT_UNWINDLIB=libgcc

%ninja_build

%install

%ninja_install -C _build
mkdir -p %{buildroot}/%{_bindir}

rm -vf %{buildroot}%{_datadir}/clang/clang-format-bbedit.applescript
rm -vf %{buildroot}%{_datadir}/clang/clang-format-sublime.py*

rm -vf %{buildroot}%{install_sharedir}/clang/clang-format-bbedit.applescript
rm -vf %{buildroot}%{install_sharedir}/clang/clang-format-sublime.py*

rm -Rvf %{buildroot}%{install_docdir}/Clang/clang/html
rm -Rvf %{buildroot}%{install_sharedir}/clang/clang-doc-default-stylesheet.css
rm -Rvf %{buildroot}%{install_sharedir}/clang/index.js
rm -vf %{buildroot}%{install_sharedir}/clang/bash-autocomplete.sh

mkdir -p %{buildroot}%{install_libdir}/clang/%{maj_ver}/{bin,include,lib,share}/

%check
%if %{with check}

LD_LIBRARY_PATH=%{buildroot}/%{install_libdir}  %{__ninja} check-all -C ./_build/
%endif

%files
%license LICENSE.TXT
%{install_bindir}/clang
%{install_bindir}/clang++
%{install_bindir}/clang-%{maj_ver}
%{install_bindir}/clang-cl
%{install_bindir}/clang-cpp
%{install_prefix}/share/man/man1/*
%if %{with classic_flang}
%{install_bindir}/flang
%endif

%files libs
%{install_libdir}/*.so.*
%{install_libdir}/clang/%{maj_ver}/include/*

%files devel
%{install_libdir}/*.so
%{install_includedir}/clang/
%{install_includedir}/clang-c/
%{install_libdir}/cmake/*
%{_bindir}/clang-tblgen
%dir %{_datadir}/clang/

%files resource-filesystem
%dir %{install_libdir}/clang/%{maj_ver}/
%dir %{install_libdir}/clang/%{maj_ver}/include/
%dir %{install_libdir}/clang/%{maj_ver}/lib/
%dir %{install_libdir}/clang/%{maj_ver}/share/
%dir %{install_libdir}/clang/

%files analyzer
%{install_libexecdir}/ccc-analyzer
%{install_libexecdir}/c++-analyzer
%{install_libexecdir}/analyze-c++
%{install_libexecdir}/analyze-cc
%{install_libexecdir}/intercept-c++
%{install_libexecdir}/intercept-cc
%{install_bindir}/scan-view
%{install_bindir}/scan-build
%{install_bindir}/analyze-build
%{install_bindir}/intercept-build
%{install_bindir}/scan-build-py
%{install_prefix}/share/man/man1/*
%{install_libdir}/libear
%{install_libdir}/libscanbuild
%{install_sharedir}/scan-view
%{install_sharedir}/scan-build


%files tools-extra
%{install_bindir}/amdgpu-arch
%{install_bindir}/c-index-test
%{install_bindir}/clang-apply-replacements
%{install_bindir}/clang-change-namespace
%{install_bindir}/clang-check
%{install_bindir}/clang-doc
%{install_bindir}/clang-extdef-mapping
%{install_bindir}/clang-format
%{install_bindir}/clang-include-cleaner
%{install_bindir}/clang-include-fixer
%{install_bindir}/clang-move
%{install_bindir}/clang-offload-bundler
%{install_bindir}/clang-offload-packager
%{install_bindir}/clang-linker-wrapper
%{install_bindir}/clang-pseudo
%{install_bindir}/clang-query
%{install_bindir}/clang-refactor
%{install_bindir}/clang-rename
%{install_bindir}/clang-reorder-fields
%{install_bindir}/clang-repl
%{install_bindir}/clang-scan-deps
%{install_bindir}/clang-tidy
%{install_bindir}/clangd
%{install_bindir}/diagtool
%{install_bindir}/hmaptool
%{install_bindir}/nvptx-arch
%{install_bindir}/pp-trace
%{install_bindir}/find-all-symbols
%{install_bindir}/modularize
%{install_bindir}/run-clang-tidy
%{install_sharedir}/clang/clang-format.el
%{install_sharedir}/clang/clang-rename.el
%{install_sharedir}/clang/clang-include-fixer.el
%{install_sharedir}/clang/clang-format.py
%{install_sharedir}/clang/clang-format-diff.py
%{install_sharedir}/clang/clang-include-fixer.py
%{install_sharedir}/clang/clang-tidy-diff.py
%{install_sharedir}/clang/run-find-all-symbols.py
%{install_sharedir}/clang/clang-rename.py

%files tools-extra-devel
%{install_includedir}/clang-tidy/

%files -n git-clang-format
%{install_bindir}/git-clang-format

%changelog
* Tue Sep 03 2024 eastb233 <xiezhiheng@huawei.com> - 17.0.6-22
- Fix build with GCC that supports SME.

* Tue Aug 20 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-21
- Fix for missing -DENABLE_AUTOTUNER in compilation.

* Tue Jul 30 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-20
- Disable toolchain_clang build for BiSheng Autotuner support temporary.

* Tue Jul 16 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-19
- Add BiSheng Autotuner support.

* Fri Jul 5 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-18
- Add toolchain_clang build support

* Tue Jun 04 2024 Zhao Mengmeng <zhaomengmeng@kylinos.cn> - 17.0.6-17
- Fix the too-early instantiation of conditional "explict" by applying the patch
  of https://github.com/llvm/llvm-project/commit/128b3b61fe6768c724975fd1df2be0abec848cf6

* Mon Apr 29 2024 wangqiang <wangqiang1@kylinos.cn> - 17.0.6-16
- Ignored the `-Wa,--generate-missing-build-notes=` option, update llvm-lit config to support macro `build_for_openeuler`

* Wed Apr 17 2024 luofeng <luofeng13@huawei.com> -17.0.6-15
- Add the support for classic flang

* Tue Apr 16 2024 wangqiang <wangqiang1@kylinos.cn> -17.0.6-14
- Avoid -Wshadow warning when init-capture named same as class field.

* Sat Apr 13 2024 liyunfei <liyunfei33@huawei.com> -17.0.6-13
- Add more warning options to BUILD_FOR_OPENEULER gcc compatible part 3.

* Tue Apr 09 2024 zhanglimin <zhanglimin@loongson.cn> - 17.0.6-12
- Improve the support for compiler-rt and fix some bugs on LoongArch.

* Fri Mar 29 2024 zhanglimin <zhanglimin@loongson.cn> -17.0.6-11
- Add the support for vector on LoongArch.

* Thu Mar 28 2024 liyunfei <liyunfei33@huawei.com> -17.0.6-10
- Add more warning options to BUILD_FOR_OPENEULER gcc compatible part 2.

* Fri Mar 15 2024 liyunfei <liyunfei33@huawei.com> -17.0.6-9
- Add more warning options to BUILD_FOR_OPENEULER gcc compatible.

* Thu Mar 14 2024 zhanglimin <zhanglimin@loongson.cn> -17.0.6-8
- Add loongarch64 to os-triple patch

* Fri Mar 1 2024 laokz <zhangkai@iscas.ac.cn> -17.0.6-7
- riscv64: backport 18.1.0 patch to disable unsupported test

* Thu Feb 22 2024 liyunfei<liyunfei33@huawei.com> -17.0.6-6
- Add BUILD_FOR_OPENEULER to clang

* Sun Feb 18 2024 liyunfei <liyunfei33@huawei.com> -17.0.6-5
- Change the default DWARF version from 5 to 4.

* Sun Feb 18 2024 laokz <zhangkai@iscas.ac.cn> -17.0.6-4
- Add riscv64 to os-triple patch

* Thu Jan 25 2024 liyunfei <liyufnei33@huawei.com> -17.0.6-3
- Add Buildrequires: llvm-libs

* Fri Dec 15 2023 liyunfei <liyunfei33@huawei.com> -17.0.6-2
- Add triples for X86_64/AArch64 openEuler gcc 

* Thu Nov 30 2023 zhoujing <zhoujing106@huawei.com> -17.0.6-1
- Update to 17.0.6 

* Thu Jul 6 2023 cf-zhao <zhaochuanfeng@huawei.com> -12.0.1-4
- Make this spec file support both system-version and multi-version.

* Thu Sep 22 2022 linguoxiong <cokelin@hnu.edu.cn> - 12.0.1-3
- Implement the "-aux-info" option to print function prototype

* Tue Aug 23 2022 linguoxiong <cokelin@hnu.edu.cn> - 12.0.1-2
- Implement some options to ignore and replace

* Wed Dec 29 2021 panxiaohe <panxiaohe@huawei.com> - 12.0.1-1
- update to 12.0.1
- add clang-resource-filesystem sub-package

* Tue Sep 07 2021 chenchen <chen_aka_jan@163.com> - 10.0.1-5
- del rpath from some binaries and bin

* Fri Apr 30 2021 licihua <licihua@huawei.com> - 10.0.1-4
- Reduce build time.

* Thu Apr 29 2021 licihua <licihua@huawei.com> - 10.0.1-3
- Reduce debuginfo verbosity.

* Thu Feb 18 2021 zhangjiapeng <zhangjiapeng9@huawei.com> - 10.0.1-2
- Modify the dependency to python3

* Fri Sep 25 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 10.0.1-1
- Delete low version dynamic library

* Thu Jul 30 2020 Guoshuai Sun <sunguoshuai> - 10.0.1-0
- Upgrade to 10.0.1

* Thu May 28 2020 leiju <leiju4@huawei.com> - 7.0.0-7
- Fix uninitialized value in ABIArgInfo

* Fri Apr 03 2020 zhouyihang <zhouyihang1@huawei.com> - 7.0.0-6
- Remove useless scriptlet

* Thu Feb 20 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.0.0-5
- Add buildrequire compiler-rt

* Tue Dec 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.0.0-4
- Delete redundant info

* Mon Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.0.0-3
- Package init
